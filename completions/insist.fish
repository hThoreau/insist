set -l cmd (basename (status -f) | cut -f 1 -d '.')
complete -c $cmd -s q -l quiet -d 'Suppress attempt count.'
complete -c $cmd -s c -l continue -d 'Continue to repeat the command line input after it\'s successful.'
complete -xc $cmd -s i -l interval -d 'Interval between attempts.'
complete -xc $cmd -s n -l attempts -d 'Set a maximum number of repetitions.'
