set -l cmd (basename (status -f) | cut -f 1 -d '.')
function $cmd -V cmd -d "Repeat the previous command line input until it succeds."

  # Variables for messages and text styling
  set -l bld (set_color --bold); set -l reg (set_color normal)
  set -l red $reg(set_color red); set -l bred (set_color --bold red)
  set -l ylw $reg(set_color yellow); set -l bylw (set_color --bold yellow)
  set -l err $red"✘ "; set -l warn $bylw"! "$ylw
  set -l instructions $bld"
DESCRIPTION"$reg"

Repeat the previous command line input, either until it is successful, or until a certain ammount of attempts are peformed.

"$bld"SYNTAX"$reg"

$cmd"$reg" [--quiet] [--continue] [--interval seconds] [--attempts number]

"$bld"OPTIONS"$reg"

-q/--quiet
Suppress attempt count.

-c/--continue
Continue to repeat the command line input after it's successful.

-i/--interval [seconds]
Interval between attempts. It can be followed by a suffix: 's' for seconds (the default), 'm' for minutes, 'h' for hours and 'd' for days.

-n/--attempts [number]
Set a maximum number of repetitions.
"
  function invalid
    echo $err"Invalid syntax."
    echo $instructions | grep -A 1 "SYNTAX"
  end

  # Test if the previous isn't insist itself
  if string match -qr -- '^insist' "$history[1]"
    echo $err"The previous command was "$bred"insist"$red" itself"
    return 1
  end

  # Parse arguments
  set -l skip; set -l quiet; set -l continue; set -l interval; set -l attempts
  for i in (seq (count $argv))
    if test -n "$skip"; set skip ''; continue; end

    if string match -qr -- \
    '^-((?=((?:([qc(i|n)])(?!.*\\1))*$))(?=.*q.*)|-quiet)' $argv[$i]
      if test -n "$quiet"; invalid; return 1; end
      set quiet true
    end

    if string match -qr -- \
    '^-((?=((?:([qc(i|n)])(?!.*\\1))*$))(?=.*c.*)|-continue$)' $argv[$i]
      if test -n "$continue"; invalid; return 1; end
      set continue true
    end

    if string match -qr -- \
    '^-((?=((?:([qci])(?!.*\\1))*$))(?=.*i.*)|-interval$)' $argv[$i]
      if test -n "$interval"; invalid; return 1
      else if string match -qvr -- '[0-9]+[smhd]?' $argv[(math $i + 1)]
        echo $instructions | grep -A 1 "-i/--interval"
        return 1
      end
      set interval $argv[(math $i + 1)]; set skip true;
    end

    if string match -qr -- \
    '^-((?=((?:([qcn])(?!.*\\1))*$))(?=.*n.*)|-attempts$)' $argv[$i]
      if test -n "$attempts"; invalid; return 1
      else if test (string match -r -- '[0-9]+' $argv[(math $i + 1)]) -lt 1 2>/dev/null
        echo $instructions | grep -A 1 "-n/--attempts"
        return 1
      end
      set attempts $argv[(math $i + 1)]; set skip true;
    end
  end

  #Main function
  set -l count 0
  while true
    set count (math $count + 1)
    test -n "$attempts"
    and set -l attempt "Attempt $count of $attempts"
    or set -l attempt "Attempt $count"$reg
    test -n "$quiet"; or echo $warn"Insisting... $attempt"$reg
    if eval "$history[1]"; test -n "$continue"; or break; end
    test $count -eq "$attempts" 2>/dev/null; and break
    test -z "$interval"; and sleep 1; or sleep $interval
  end
end
